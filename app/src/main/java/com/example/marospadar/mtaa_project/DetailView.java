package com.example.marospadar.mtaa_project;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.marospadar.mtaa_project.backendless.BackendlessRequest;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;
import java.util.Objects;

import io.socket.client.Ack;


public class DetailView extends AppCompatActivity implements ObservableScrollViewCallbacks {



    RequestQueue requestQueue;
    ProgressDialog progress;

    GetImages getImages;

    LoadingData loadingData;

    final boolean[] received = {false};
    final boolean[] serverError = {false};

    JSONArray submissions;
    JSONArray posts = new JSONArray();

    SQLiteDatabase myDatabase;

    int imagesCount = 0;
    int savedImagesCount = 0;

    String objectId;
    String categoryIntent;

    TextView price;
    TextView title;
    TextView description;
    TextView category;
    TextView person;
    TextView location;
    TextView date;
    TextView phone;
    ImageView picture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_view);

        price = (TextView) findViewById(R.id.itemPrice);
        title = (TextView) findViewById(R.id.itemTitle);
        description = (TextView) findViewById(R.id.descTextView);
        category = (TextView) findViewById(R.id.itemCategory);
        person = (TextView) findViewById(R.id.personTextView);
        location = (TextView) findViewById(R.id.locationTextView);
        date = (TextView) findViewById(R.id.dateTextView);
        phone = (TextView) findViewById(R.id.phoneTextView);
        picture = (ImageView) findViewById(R.id.itemPicture);

        ObservableScrollView listView = (ObservableScrollView) findViewById(R.id.scrollView);
        listView.setScrollViewCallbacks(this);

        Intent i = getIntent();

        // Selected image id
        objectId = i.getExtras().getString("objectID");
        categoryIntent = i.getExtras().getString("category");

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        loadData();
    }

    public void pictureListener(View view)
    {
        Intent i = new Intent(getApplicationContext(), DetailImageView.class);
        i.putExtra("ObjectId" , objectId);
        startActivity(i);
    }

    //IBA OVERRIDNUTE FUNKCIE SCHOVAVANIA ACTION BARU
    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll,
                                boolean dragging) {
    }

    @Override
    public void onDownMotionEvent() {
    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
        ActionBar ab = getSupportActionBar();
        if (scrollState == ScrollState.UP) {
            if (ab.isShowing()) {
                ab.hide();
            }
        } else if (scrollState == ScrollState.DOWN) {
            if (!ab.isShowing()) {
                ab.show();
            }
        }
    }


    protected void loadData(){

        ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo ni = cm.getActiveNetworkInfo();
        //ak zariadenie nie je pripojene na internet obnovia sa offline data a ulozia sa do premennych typu arraylist
        if (ni == null || (!SocketHandler.getSocket().connected())) {
            myDatabase = this.openOrCreateDatabase("Items", MODE_PRIVATE, null);

            Cursor c = myDatabase.rawQuery("SELECT objectId,category,price,title,phoneNumber,name,location,description,creationDate, path  FROM items WHERE objectId=" + "'" + objectId + "'", null);

            int category_db = c.getColumnIndex("category");
            int price_db = c.getColumnIndex("price");
            int title_db = c.getColumnIndex("title");
            int phoneNumber_db = c.getColumnIndex("phoneNumber");
            int name_db = c.getColumnIndex("name");
            int location_db = c.getColumnIndex("location");
            int description_db = c.getColumnIndex("description");
            int creationDate_db = c.getColumnIndex("creationDate");
            int path_db = c.getColumnIndex("path");

            String path;

            if (c != null && c.moveToFirst()){
                do {
                    price.setText(c.getString(price_db)+" €");
                    description.setText(c.getString(description_db));
                    title.setText(c.getString(title_db));
                    category.setText(c.getString(category_db));
                    person.setText(c.getString(name_db));
                    location.setText(c.getString(location_db));
                    date.setText(c.getString(creationDate_db));
                    phone.setText(c.getString(phoneNumber_db));

                    path = c.getString(path_db);
                    Bitmap bmp = BitmapFactory.decodeFile(path);
                    picture.setImageBitmap(bmp);


                } while (c.moveToNext());
            }
        }
        //ak zariadenie je online stiahnu sa najnovsie data
        else {
            progress = ProgressDialog.show(DetailView.this, "Loading", "Loading Data", true);
            loadingData = new LoadingData();
            loadingData.execute((Void) null);
        }
    }


    public void loadDataFromServer2(){

        JSONObject obj = new JSONObject();

        try {
            obj.put("url", "/data/Mattafix1/"+objectId);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        SocketHandler.getSocket().emit("get", obj, new Ack() {
            @Override
            public void call(Object... args) {
                JSONObject obj = (JSONObject) args[0];
                JSONObject body = null;
                JSONArray data = null;


                try {
                    body = obj.getJSONObject("body");
                    data = body.getJSONArray("data");
                    final JSONObject dataFromArray;
                    dataFromArray = data.getJSONObject(0);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                price.setText(dataFromArray.getString("Price") + " €");
                                description.setText(dataFromArray.getString("Description"));
                                title.setText(dataFromArray.getString("Title"));
                                category.setText(dataFromArray.getString("Category"));
                                person.setText(dataFromArray.getString("Name"));
                                location.setText(dataFromArray.getString("Location"));
                                date.setText(dataFromArray.getString("created"));
                                phone.setText(dataFromArray.getString("PhoneNumber"));
                                String url = dataFromArray.getString("Photo");
                                ifFileExist(url);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });


                    received[0] = true;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    public void ifFileExist(String url){

        File extStore = Environment.getExternalStorageDirectory();
        File myFile = new File(extStore.getAbsolutePath() + "/Photos/"+objectId+".jpg");

        if(myFile.exists()){
            String path = myFile.getAbsolutePath();
            Bitmap bmp = BitmapFactory.decodeFile(path);
            picture.setImageBitmap(bmp);
        }
        else{
            getImages = new GetImages(url);
            getImages.execute((Void) null);
        }
    }


    public class LoadingData extends AsyncTask<Void, Void, Boolean> {

        LoadingData() {
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            if (received[0] == true) {

            } else {
                loadDataFromServer2();
            }
            //tento while je preto, aby appka cakala pokym nepridu data zo servera
            while (received[0] != true) {

            }
            if (serverError[0] == true) {
                return false;
            } else {
                return true;
            }

        }

        @Override
        protected void onPostExecute(final Boolean success) {
            if (success) {
                loadingData = null;
                progress.dismiss();

            } else {
                progress.dismiss();
                AlertDialog alertDialog = new AlertDialog.Builder(DetailView.this).create();
                alertDialog.setTitle("Error While Loading server data");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                serverError[0] = true;
                            }
                        });
                alertDialog.show();
            }
        }
    }

    private class GetImages extends AsyncTask<Object, Object, Object> {
        private String requestUrl, imagename_;
        private ImageView view;
        private Bitmap bitmap ;
        private FileOutputStream fos;

        private GetImages(String requestUrl) {
            this.requestUrl = requestUrl;
        }

        @Override
        protected Bitmap doInBackground(Object... objects) {
            try {
                URL url = new URL(requestUrl);
                URLConnection conn = url.openConnection();
                bitmap = BitmapFactory.decodeStream(conn.getInputStream());
            } catch (Exception ex) {
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Object o) {
            if(o != null)
            {
                picture.setImageBitmap(bitmap);
            }
            else {
               //error while get picture
            }
        }
    }


    //MENU
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_with_edit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.action_settings_edit:
                Intent i = new Intent(getApplicationContext(), EditItem.class);
                if (!objectId.equals("null")) {
                    i.putExtra("objectID", objectId);
                    i.putExtra("category", categoryIntent);
                    startActivity(i);
                }
                else if(objectId.equals("null")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage("You are attempting to edit data which was created offline, for edit please click OK button and refresh data from server.")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent i = new Intent(getApplicationContext(), CategoryActivity.class);
                                    startActivity(i);
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                    break;
                }
                break;
            case R.id.action_settings_logout:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Are you sure you want to exit application?")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                SocketHandler.getSocket().disconnect();
                                startActivity(i);
                            }
                        })
                        .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
                break;
            case R.id.deleteItem:
                // TODO Auto-generated method stub

                if (!objectId.equals("null")) {
                    builder = new AlertDialog.Builder(DetailView.this);
                    builder.setMessage("Do you really want to delete item ")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    progress = ProgressDialog.show(DetailView.this, "Delete", "Deleting Item", true);
                                    deleteItem();
                                    while (received[0] != true) {
                                    }
                                    progress.dismiss();
                                    Toast.makeText(DetailView.this, "Item was deleted!", Toast.LENGTH_LONG).show();
                                    Intent i = new Intent(getApplicationContext(), CategoryActivity.class);
                                    startActivity(i);
                                    finish();


                                }
                            })
                            .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    alert = builder.create();
                    alert.show();
                }
                else if (objectId.equals("null")){
                    builder = new AlertDialog.Builder(DetailView.this);
                    builder.setMessage("You are attempting to delete data which was created offline, for delete please click OK button and refresh data from server.")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent i = new Intent(getApplicationContext(), CategoryActivity.class);
                                    startActivity(i);
                                }
                            });
                    alert = builder.create();
                    alert.show();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void deleteItem()
    {
        JSONObject obj = new JSONObject();
        try {
            obj.put("url", "/data/Mattafix1/" + objectId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo ni = cm.getActiveNetworkInfo();
        //ak zariadenie nie je pripojene na internet obnovia sa offline data a ulozia sa do premennych typu arraylist
        if (ni == null || (!SocketHandler.getSocket().connected())){
            final DataSingleton dataSingleton = DataSingleton.getInstance();
            myDatabase.execSQL("DELETE FROM items WHERE objectId= " + "'"+objectId+"'");
            dataSingleton.setDataForDELETE(dataSingleton.addtolistDELETE(obj));
            received[0] = true;
        }
        else {
            SocketHandler.getSocket().emit("delete", obj, new Ack() {
                @Override
                public void call(Object... args) {
                    received[0] = true;
                }
            });
        }
    }

}
