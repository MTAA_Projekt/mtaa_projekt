package com.example.marospadar.mtaa_project;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.view.LayoutInflater;


import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import io.socket.client.Ack;

public class AddItem extends AppCompatActivity implements ObservableScrollViewCallbacks {

    EditText price;
    EditText title;
    EditText description;
    TextView category;
    EditText person;
    EditText location;
    EditText phone;
    TextView date;
    ImageView picture;

    GetImages getImages;
    GetImagesForOfflineADD getImagesForOfflineADD;

    final boolean[] received = {false};
    final boolean[] serverError = {false};
    boolean loadedPicture = false;

    public String photoURL = "https://api.backendless.com/574DDB0E-100F-5C2B-FF55-3052A0F85C00/v1/files/Photos/";
    public String photoURL_control = "https://api.backendless.com/574DDB0E-100F-5C2B-FF55-3052A0F85C00/v1/files/Photos/";
    public String photoFinal = "https://api.backendless.com/574DDB0E-100F-5C2B-FF55-3052A0F85C00/v1/files/Photos/";;

    RequestQueue requestQueue;
    ProgressDialog progress;
    PostingData postingData;

    SQLiteDatabase myDatabase;
    String objectId;

    String categoryIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);

        ObservableScrollView scrollView = (ObservableScrollView) findViewById(R.id.scrollView);
        scrollView.setScrollViewCallbacks(this);

        Button addButton = (Button) findViewById(R.id.AddButton);
        addButton.setOnClickListener(addButtonListener());

        Button addPhotoButton = (Button) findViewById(R.id.addPhotoButton);
        addPhotoButton.setOnClickListener(addPhotoButtonListener());

        Bundle extras = getIntent().getExtras();
        categoryIntent =  extras.getString("Category");

        picture = (ImageView) findViewById(R.id.itemPicture);
        category = (TextView) findViewById(R.id.itemCategoryAdd);
        price = (EditText) findViewById(R.id.itemPriceAdd);
        title = (EditText) findViewById(R.id.itemTitleAdd);
        description = (EditText) findViewById(R.id.descTextViewAdd);
        person = (EditText) findViewById(R.id.personTextViewAdd);
        location = (EditText) findViewById(R.id.locationTextViewAdd);
        phone = (EditText) findViewById(R.id.phoneTextViewAdd);

        category.setText(categoryIntent);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    protected void postData(){

        if(isValuesValid(price.getText().toString(),
                title.getText().toString(),description.getText().toString(),person.getText().toString(),location.getText().toString(),
                phone.getText())) {

            //post ale predtym vytvorenie JSONObjektu ktory sa bude odosielat
            ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo ni = cm.getActiveNetworkInfo();
            //ak zariadenie nie je pripojene na internet obnovia sa offline data a ulozia sa do premennych typu arraylist
            if (ni == null || (!SocketHandler.getSocket().connected())) {

                myDatabase = this.openOrCreateDatabase("Items", MODE_PRIVATE, null);
                JSONObject obj = new JSONObject();

                try {
                    JSONObject jso = new JSONObject();
                    JSONArray jarr1=new JSONArray();
                    jso.put("Category", categoryIntent);
                    jso.put("Description", description.getText().toString());
                    jso.put("created", getCurrentDateTime().toString());
                    jso.put("Title", title.getText().toString());
                    jso.put("Photo", photoFinal);
                    jso.put("Name", person.getText().toString());
                    jso.put("Price", price.getText().toString());
                    jso.put("PhoneNumber", phone.getText().toString());
                    jso.put("Location", location.getText().toString());

                    jarr1.put(0, jso);

                    obj.put("url", "/data/Mattafix1");
                    obj.put("data", new JSONObject().put("data", jarr1));


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final DataSingleton dataSingleton = DataSingleton.getInstance();

                getImagesForOfflineADD = new GetImagesForOfflineADD(photoFinal, objectId);
                getImagesForOfflineADD.execute((Void) null);

                myDatabase.execSQL("INSERT INTO items (title, price, phoneNumber, photo, name, location, description, creationdate, category, objectId) VALUES "
                        + "(" + "'" + title.getText().toString() + "'" + "," + "'" + price.getText().toString() + "'" + "," + "'" + phone.getText().toString() + "'" + "," + "'" + photoFinal + "'" + "," + "'" + person.getText().toString() + "'" + "," + "'" + location.getText().toString() + "'" + "," + "'" + description.getText().toString() + "'" + "," + "'" + getCurrentDateTime().toString() + "'" + "," + "'" + categoryIntent + "'" + "," + "'" + objectId + "'" + ")");

                dataSingleton.setDataForPOST(dataSingleton.addtolistPOST(obj));
                return;
            }
            //ak zariadenie je online stiahnu sa najnovsie data
            else {
                //ked sa vratime z predch. screeny nech nenacitava data znovu
                requestQueue = VolleySingleton.getInstance(this).getRequestQueue();
                //nacitaj data zo servera a uloz ich do DB
                progress = ProgressDialog.show(AddItem.this, "Posting", "Posting Data", true);
                postingData = new PostingData();
                postingData.execute((Void) null);
            }
        }
    }

    public void postItemToServer2() throws ParseException, JSONException {

        JSONObject obj = new JSONObject();

        try {
            JSONObject jso = new JSONObject();
            JSONArray jarr1=new JSONArray();
            jso.put("Category", categoryIntent);
            jso.put("Description", description.getText().toString());
            jso.put("created", getCurrentDateTime().toString());
            jso.put("Title", title.getText().toString());
            jso.put("Photo", photoFinal);
            jso.put("Name", person.getText().toString());
            jso.put("Price", price.getText().toString());
            jso.put("PhoneNumber", phone.getText().toString());
            jso.put("Location", location.getText().toString());

            jarr1.put(0, jso);

            obj.put("url", "/data/Mattafix1");
            obj.put("data", new JSONObject().put("data", jarr1));


        } catch (JSONException e) {
            e.printStackTrace();
        }

        SocketHandler.getSocket().emit("post", obj, new Ack() {
            @Override
            public void call(Object... args) {
                try {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(AddItem.this, "Item: " + title.getText().toString() + " was created!", Toast.LENGTH_LONG).show();
                            received[0] = true;
                            Intent i = new Intent(getApplicationContext(), CategoryActivity.class);
                            startActivity(i);
                            finish();
                        }
                    });
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public View.OnClickListener addButtonListener()
    {
        return new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                postData();
            }
        };
    }

    public View.OnClickListener addPhotoButtonListener()
    {
        return new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                // get prompts.xml view
                LayoutInflater li = LayoutInflater.from(AddItem.this);
                View promptsView = li.inflate(R.layout.prompts, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        AddItem.this);

                // set prompts.xml to alertdialog builder
                alertDialogBuilder.setView(promptsView);

                final EditText userInput = (EditText) promptsView
                        .findViewById(R.id.editTextDialogUserInput);

                // set dialog message
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        // get user input and set it to photoURL
                                        // edit text

                                        photoFinal = photoURL.concat(userInput.getText().toString());
                                        if (!photoFinal.equals(photoURL_control)){
                                            getImages = new GetImages(photoFinal);
                                            getImages.execute((Void) null);
                                            photoURL = "https://api.backendless.com/574DDB0E-100F-5C2B-FF55-3052A0F85C00/v1/files/Photos/";
                                        }
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        dialog.cancel();
                                    }
                                });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();

            }
        };
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll,
                                boolean dragging) {
    }

    @Override
    public void onDownMotionEvent() {
    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
        ActionBar ab = getSupportActionBar();
        if (scrollState == ScrollState.UP) {
            if (ab.isShowing()) {
                ab.hide();
            }
        } else if (scrollState == ScrollState.DOWN) {
            if (!ab.isShowing()) {
                ab.show();
            }
        }
    }

    //MENU
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    //PRAVE HORNE MENU CLICK
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.action_settings_logout:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Do you want to leave adding Item without saving changes ?")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(i);
                                finish();
                            }
                        })
                        .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();

        }
        return super.onOptionsItemSelected(item);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    protected void exitByBackKey() {

        AlertDialog alertbox = new AlertDialog.Builder(this)
                .setMessage("Do you want to leave adding Item without saving changes ?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                })
                .show();

    }

    public class PostingData extends AsyncTask<Void, Void, Boolean> {

        PostingData() {
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            if (received[0] == true) {
            } else {
                try {
                    postItemToServer2();
                } catch (ParseException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }            }
            //tento while je preto, aby appka cakala pokym nepridu data zo servera
            while (received[0] != true) {

            }

            if (serverError[0] == true) {
                return false;
            } else {
                return true;
            }
        }
    }

    public String getCurrentDateTime() {

        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        //get current date time with Date()
        Date date = new Date();
        String dateString = dateFormat.format(date).toString();

        return dateString;
    }

    public boolean isValuesValid( String price, String title, String description, String person, String location,CharSequence phone )
    {
        return Validate.isNumber(this, price) && Validate.isEmptyString(this, title)
                && Validate.isEmptyString(this, description) && Validate.isEmptyString(this, person)
                && Validate.isEmptyString(this, location) && Validate.isValidPhoneNumber(this, phone);
    }

    private class GetImages extends AsyncTask<Object, Object, Object> {
        private String requestUrl, imagename_;
        private ImageView view;
        private Bitmap bitmap ;
        private FileOutputStream fos;

        private GetImages(String requestUrl) {
            this.requestUrl = requestUrl;
        }

        @Override
        protected Bitmap doInBackground(Object... objects) {
            try {
                URL url = new URL(requestUrl);
                URLConnection conn = url.openConnection();
                bitmap = BitmapFactory.decodeStream(conn.getInputStream());
            } catch (Exception ex) {
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Object o) {
            if(o != null)
            {
                picture.setImageBitmap(bitmap);
            }
            else {
                //error while get picture
            }
        }
    }

    private class GetImagesForOfflineADD extends AsyncTask<Object, Object, Object> {
        private String requestUrl, imagename_;
        private ImageView view;
        private Bitmap bitmap ;
        private FileOutputStream fos;

        private GetImagesForOfflineADD(String requestUrl, String _imagename_) {
            this.requestUrl = requestUrl;
            this.imagename_ = _imagename_ ;
        }

        @Override
        protected Object doInBackground(Object... objects) {
            try {
                URL url = new URL(requestUrl);
                URLConnection conn = url.openConnection();
                bitmap = BitmapFactory.decodeStream(conn.getInputStream());

            } catch (Exception ex) {
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            if (!ImageStorage.checkifImageExists(imagename_)) {
                //view.setImageBitmap(bitmap);
                ImageStorage.saveToSdCard(bitmap, imagename_, myDatabase);
                Toast.makeText(AddItem.this, "Item: " + title.getText().toString() + " was created!", Toast.LENGTH_LONG).show();
                received[0] = true;
                Intent i = new Intent(getApplicationContext(), CategoryActivity.class);
                startActivity(i);
                finish();
            } else {
                Toast.makeText(AddItem.this, "Item: " + title.getText().toString() + " was created!", Toast.LENGTH_LONG).show();
                received[0] = true;
                Intent i = new Intent(getApplicationContext(), CategoryActivity.class);
                startActivity(i);
                finish();
            }

        }
    }



}
