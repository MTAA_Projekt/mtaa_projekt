package com.example.marospadar.mtaa_project;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.marospadar.mtaa_project.backendless.BackendlessRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Map;

import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class BikeList extends AppCompatActivity {

    private GridView gridView;
    private GridViewAdapter gridAdapter;

    ArrayList<String> paths = new ArrayList<String>();// list of file paths

    JSONArray posts = new JSONArray();

    ProgressDialog progress;
    JSONArray submissions;

    final boolean[] received = {false};

    LoadingData loadingData;
    RequestQueue requestQueue;

    SQLiteDatabase myDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bike_list);

        requestQueue = VolleySingleton.getInstance(this).getRequestQueue();

        ActionBar actionBar = getActionBar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle extras = getIntent().getExtras();

        final String selectedCategory = extras.getString("Category");

        if (selectedCategory.contains("scooter")){
            setTitle("SCOOTERS");
            displayGridView("scooter");

            //getDataOnline();
        }
        if (selectedCategory.contains("enduro")){
            setTitle("ENDURO");
            displayGridView("enduro");
            //getDataOnline("enduro");

        }
        if (selectedCategory.contains("roadBike")){
            setTitle("ROAD BIKES");
            displayGridView("roadBike");
            //getDataOnline("roadBike");

        }
        if (selectedCategory.contains("quadBike")){
            setTitle("QUAD BIKES");
            displayGridView("quadBike");
            //getDataOnline("quadBike");
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AddItem.class);
                intent.putExtra("Category", selectedCategory);
                startActivity(intent);
               /* Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
            }
        });

        /*SocketHandler.getSocket().on(Socket.EVENT_CONNECT, new Emitter.Listener() {

            @Override
            public void call(Object... args) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        AlertDialog.Builder builder = new AlertDialog.Builder(BikeList.this);
                        builder.setMessage("Internet connection available. Do you want to load latest server data ?")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Intent i = new Intent(getApplicationContext(), CategoryActivity.class);
                                        startActivity(i);
                                        finish();

                                    }
                                })
                                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();
                    }
                });
            }

        });*/
    }


    //DISPLAY PICTURES INTO GRID VIEW
    public void displayGridView (final String category2){

        myDatabase = this.openOrCreateDatabase("Items", MODE_PRIVATE, null);

        Cursor c = myDatabase.rawQuery("SELECT objectId,category,price,title FROM items WHERE category=" + "'" + category2 + "'", null);

        int objectId = c.getColumnIndex("objectId");
        int price = c.getColumnIndex("price");
        int title = c.getColumnIndex("title");

        JSONObject obj = null;

        if (c != null && c.moveToFirst()){
            do {
                try {
                    obj = new JSONObject();
                    obj.put("ObjectId",c.getString(objectId));
                    obj.put("Price",c.getString(price));
                    obj.put("Title",c.getString(title));

                    posts.put(obj);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } while (c.moveToNext());
        }

        //grid view nastavenie
        gridView = (GridView) findViewById(R.id.gridView);
        gridAdapter = new GridViewAdapter(this, R.layout.bike_list_grid_item_layout, getData());
        gridView.setAdapter(gridAdapter);
        //on click item
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                ImageItem item = (ImageItem) parent.getItemAtPosition(position);


                Intent intent = new Intent(getApplicationContext(), DetailView.class);
                intent.putExtra("objectID", item.getObjectID());
                intent.putExtra("category", category2);
                startActivity(intent);
            }
        });
        gridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View v, int position, long id) {
                // TODO Auto-generated method stub
                final ImageItem item = (ImageItem) parent.getItemAtPosition(position);


                if (!item.getObjectID().equals("null")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(BikeList.this);
                    builder.setMessage("Do you really want to delete item " + item.getTitle())
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    progress = ProgressDialog.show(BikeList.this, "Delete", "Deleting Item", true);
                                    deleteItem(item.getObjectID());
                                    while (received[0] != true) {
                                    }
                                    progress.dismiss();
                                    received[0] = false;
                                    Toast.makeText(BikeList.this, "Item was deleted!", Toast.LENGTH_LONG).show();
                                    Intent i = new Intent(getApplicationContext(), CategoryActivity.class);
                                    startActivity(i);
                                    finish();

                                }
                            })
                            .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
                else if (item.getObjectID().equals("null")){
                    AlertDialog.Builder builder = new AlertDialog.Builder(BikeList.this);
                    builder.setMessage("You are attempting to delete data which was created offline, for delete please click OK button and refresh data from server.")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent i = new Intent(getApplicationContext(), CategoryActivity.class);
                                    startActivity(i);
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
                return false;
            }
        });
    }

    //ROZDELENIE FOTIEK na SD podla object ID (tuto uz mame object id pre specificku kategoriu)
    public void getFromSdcard()
    {
        File[] listFile;
        File file= new File(android.os.Environment.getExternalStorageDirectory(),"Photos");
        if (file.isDirectory())
        {
            listFile = file.listFiles();
            JSONObject submission;
            for (int i = 0; i < listFile.length; i++) {
                for (int j = 0; j < posts.length(); j++) {
                    try {
                        submission = posts.getJSONObject(j);

                        String objectId = submission.getString("ObjectId");
                        String withExtension = listFile[i].getName();
                        String filenameArray[] = withExtension.split("\\.");
                        String nameOfFile = filenameArray[0];

                        if (objectId.equals(nameOfFile)){

                            paths.add(listFile[i].getAbsolutePath());
                            continue;

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
    //GET DATA - zabezpecuje ziskanie ciest k fotkam pre zobrazenie v grid view
    private ArrayList<ImageItem> getData() {

        final ArrayList<ImageItem> imageItems = new ArrayList<>();

        getFromSdcard();

        for (int i = 0; i < paths.size(); i++) {

            String objectId = paths.get(i);

            myDatabase = this.openOrCreateDatabase("Items", MODE_PRIVATE, null);
            String path = paths.get(i);

            String updatedPath = path.substring(path.lastIndexOf("/") + 1);
            String filenameArray[] = updatedPath.split("\\.");
            String nameOfFile = filenameArray[0];

            Cursor c = myDatabase.rawQuery("SELECT price,title,location FROM items WHERE objectId= " + "'"+nameOfFile+"'", null);

            int location = c.getColumnIndex("location");
            int price = c.getColumnIndex("price");
            int title = c.getColumnIndex("title");

            JSONObject obj = null;

            String showTitle = null;
            String showPrice = null;
            String showLocation = null;

            if (c != null && c.moveToFirst()){
                do {
                    showTitle = c.getString(title);
                    showPrice = c.getString(price);
                    showLocation = c.getString(location);

                } while (c.moveToNext());
            }


            Bitmap bitmap = BitmapFactory.decodeFile(paths.get(i));
            imageItems.add(new ImageItem(bitmap, showTitle, showPrice, nameOfFile));
        }
        return imageItems;
    }

    //PRAVE HORNE MENU
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    //PRAVE HORNE MENU CLICK
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.action_settings_logout:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Are you sure you want to exit application?")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                SocketHandler.getSocket().disconnect();
                                startActivity(i);
                            }
                        })
                        .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();

        }
        return super.onOptionsItemSelected(item);
    }

    public class LoadingData extends AsyncTask<Void, Void, Boolean> {

        LoadingData() {}

        @Override
        protected Boolean doInBackground(Void... params) {

            //getDataOnline();

            while (received[0] != true){
            }
            Log.i("Infor", "Neco");
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            if (success){
                loadingData = null;

                progress.dismiss();
            }
            else{
            }
        }
        @Override
        protected void onCancelled() {
            loadingData = null;
            progress.dismiss();
        }
    }

    public void deleteItem(String objectId)
    {
        JSONObject obj = new JSONObject();
        try {
            obj.put("url", "/data/Mattafix1/" + objectId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo ni = cm.getActiveNetworkInfo();
        //ak zariadenie nie je pripojene na internet obnovia sa offline data a ulozia sa do premennych typu arraylist
        if (ni == null || (!SocketHandler.getSocket().connected())){
            final DataSingleton dataSingleton = DataSingleton.getInstance();
            myDatabase.execSQL("DELETE FROM items WHERE objectId= " + "'"+objectId+"'");
            dataSingleton.setDataForDELETE(dataSingleton.addtolistDELETE(obj));
            received[0] = true;
        }
        else {
            SocketHandler.getSocket().emit("delete", obj, new Ack() {
                @Override
                public void call(Object... args) {
                    received[0] = true;
                }
            });
        }
    }
}
