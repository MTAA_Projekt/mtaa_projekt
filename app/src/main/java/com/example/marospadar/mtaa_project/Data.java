package com.example.marospadar.mtaa_project;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Martin on 31/03/16.
 */
public class Data {

    private static Data sharedInstance = new Data();

    private Data() {

    }

    public static Data sharedInstance(){
        return sharedInstance;
    }

    ArrayList<JSONObject> scooterResult = new ArrayList<JSONObject>();
    ArrayList<JSONObject> enduroResult = new ArrayList<JSONObject>();
    ArrayList<JSONObject> quadBikeResult = new ArrayList<JSONObject>();
    ArrayList<JSONObject> roadBikeResult = new ArrayList<JSONObject>();

}
