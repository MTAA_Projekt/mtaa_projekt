package com.example.marospadar.mtaa_project;

/**
 * Created by marospadar on 17.04.16.
 */

import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class DataSingleton  {

    public static DataSingleton getDataSingleton() {
        return dataSingleton;
    }

    public static void setDataSingleton(DataSingleton dataSingleton) {
        DataSingleton.dataSingleton = dataSingleton;
    }

    private static DataSingleton dataSingleton = new DataSingleton();

    private ArrayList<JSONObject> dataForPUT;
    private ArrayList<JSONObject> dataForPOST;
    private ArrayList<JSONObject> dataForDELETE;

    private DataSingleton(){}

    public static DataSingleton getInstance(){
        return dataSingleton;
    }


    public ArrayList<JSONObject> getDataForPUT() {
        return dataForPUT;
    }
    public ArrayList<JSONObject> getDataForPOST() {
        return dataForPOST;
    }
    public ArrayList<JSONObject> getDataForDELETE() {
        return dataForDELETE;
    }


    public void setDataForPUT(ArrayList<JSONObject> dataForUpload) {
        this.dataForPUT = dataForUpload;
    }

    public void setDataForPOST(ArrayList<JSONObject> dataForUpload) {
        this.dataForPOST = dataForUpload;
    }

    public void setDataForDELETE(ArrayList<JSONObject> dataForUpload) {
        this.dataForDELETE = dataForUpload;
    }


    public static Boolean BackgroundColor;
    {
        BackgroundColor = true;
    }


    ArrayList<JSONObject> arrayListPUT = new ArrayList<>();
    ArrayList<JSONObject> arrayListPOST = new ArrayList<>();
    ArrayList<JSONObject> arrayListDELETE = new ArrayList<>();

    public ArrayList<JSONObject> addtolistPUT(JSONObject jsonObjectRequest)
    {
        arrayListPUT.add(jsonObjectRequest);
        return arrayListPUT;
    }

    public ArrayList<JSONObject> addtolistPOST(JSONObject jsonObjectRequest)
    {
        arrayListPOST.add(jsonObjectRequest);
        return arrayListPOST;
    }

    public ArrayList<JSONObject> addtolistDELETE(JSONObject jsonObjectRequest)
    {
        arrayListDELETE.add(jsonObjectRequest);
        return arrayListDELETE;
    }

}