package com.example.marospadar.mtaa_project;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

import android.util.Log;
import android.util.LruCache;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.HashMap;

/**
 * Created by Martin on 24/03/16.
 */
public class VolleySingleton {

    private static VolleySingleton instance;
    private RequestQueue requestQueue;
    private ImageLoader imageLoader;

    public static final String APPLICATION_ID = "574DDB0E-100F-5C2B-FF55-3052A0F85C00";
    public static final String SECRET_KEY = "3A8F2C61-FBD7-B2C4-FF86-08D830467A00";
    public static final String APPLICATION_TYPE = "REST";
    public static final String CONTENT_TYPE = "application/json; charset=utf-8";

    private VolleySingleton(Context context) {
        requestQueue = Volley.newRequestQueue(context);

        imageLoader = new ImageLoader(requestQueue, new ImageLoader.ImageCache() {
            private final LruCache<String, Bitmap> cache = new LruCache<String, Bitmap>(20);


            @Override
            public Bitmap getBitmap(String url) {
                return cache.get(url);
            }

            @Override
            public void putBitmap(String url, Bitmap bitmap) {
                cache.put(url, bitmap);
            }
        });
    }


    public static VolleySingleton getInstance(Context context) {
        if (instance == null) {
            instance = new VolleySingleton(context);
        }
        return instance;
    }

    public RequestQueue getRequestQueue() {
        return requestQueue;
    }

    public ImageLoader getImageLoader() {
        return imageLoader;
    }

    public static String trimMessage(String json, String key){
        String trimmedString = null;

        try{
            JSONObject obj = new JSONObject(json);
            trimmedString = obj.getString(key);
        } catch(JSONException e){
            e.printStackTrace();
            return null;
        }

        return trimmedString;
    }

    public static void displayMessage(String toastString, Context context){
        Toast.makeText(context, toastString, Toast.LENGTH_LONG).show();
    }

    public static JSONObject bodyForPost(String photo, String category, String price, String creationDate,String title, String description, String person, String location, String phone) throws JSONException, ParseException {
        JSONObject jsonObject = new JSONObject();

        if(!photo.equals(""))
        {
            jsonObject.put("Photo", photo);
        }
        if(!category.equals(""))
        {
            jsonObject.put("Category", category);
        }
        if(!price.equals(""))
        {
            jsonObject.put("price", price);
        }
        if(!creationDate.equals(""))
        {
            jsonObject.put("CreationDate", creationDate);
        }

        if(!title.equals(""))
        {
            jsonObject.put("title", title);
        }

        if(!description.equals(""))
        {
            jsonObject.put("description",description);
        }

        if(!person.equals(""))
        {
            jsonObject.put("Name", person);
        }

        if(!location.equals(""))
        {
            jsonObject.put("location", location);
        }

        if (!phone.equals(""))
        {
            jsonObject.put("PhoneNumber", phone);
        }


        return jsonObject;

    }

    public  static HashMap<String,String> header()
    {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("application-id", APPLICATION_ID);
        headers.put("secret-key", SECRET_KEY);
        headers.put("Content-Type", CONTENT_TYPE);
        headers.put("application-type", APPLICATION_TYPE);
        return headers;
    }
    public  static HashMap<String,String> headerForDelete()
    {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("application-id", APPLICATION_ID);
        headers.put("secret-key", SECRET_KEY);

        return headers;
    }
}

