package com.example.marospadar.mtaa_project;

import android.graphics.Bitmap;

/**
 * Created by Martin on 30/03/16.
 */
public class ImageItem {
    private Bitmap image;
    private String title;



    private String price;
    private String objectID;



    public ImageItem(Bitmap image, String title, String price, String objectID) {
        super();
        this.image = image;
        this.title = title;
        this.price = price;
        this.objectID = objectID;
    }

    public String getObjectID() {
        return objectID;
    }

    public void setObjectID(String objectID) {
        this.objectID = objectID;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
