package com.example.marospadar.mtaa_project;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.android.volley.*;

import android.content.Intent;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import io.socket.client.Ack;
import io.socket.client.Socket;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.*;

import java.io.Console;
import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.emitter.Emitter;

public class MainActivity extends AppCompatActivity {

    RequestQueue requestQueue;
    JSONObject resultItems;

    private UserLoginTask mAuthTask = null;

    ProgressDialog progress;

    final boolean[] received = {false};

    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "jozko@gmail.com:pass",
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button loginButton = (Button) findViewById(R.id.loginButton);
        loginButton.setOnClickListener(createLoginButtonListener());

        //options pre socket io connection
        IO.Options opts = new IO.Options();
        opts.secure = false;
        opts.port = 1341;
        opts.reconnection = true;
        opts.forceNew = true;
        opts.timeout = 5000;

        //pripojenie na server a ulozenie do triedy Socket Handler
        try {
            SocketHandler.setSocket(IO.socket("http://sandbox.touch4it.com:1341/?__sails_io_sdk_version=0.12.1", opts));
            SocketHandler.getSocket().connect();

        }
        catch (URISyntaxException e) {}

/*
        IO.Options opts = new IO.Options();
        opts.secure = false;
        opts.port = 1341;
        opts.reconnection = true;
        opts.forceNew = true;
        opts.timeout = 5000;*/
        /*
        try {
            socket = IO.socket("http://sandbox.touch4it.com:1341/?__sails_io_sdk_version=0.12.1", opts);
            socket.connect();
        }
        catch (URISyntaxException e) {}


        JSONObject obj = new JSONObject();

        try {
            obj.put("url", "/data/Mattafix/872ee1cc-ed3b-4427-83c6-f577ecdde654");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        socket.emit("delete", obj, new Ack() {
            @Override
            public void call(Object... args) {
                JSONObject obj = (JSONObject) args[0];
            }
        });*/
        /*
        try {
            socket = IO.socket("http://sandbox.touch4it.com:1341/?__sails_io_sdk_version=0.12.1", opts);
            socket.connect();
        }
        catch (URISyntaxException e) {}


        JSONObject obj = new JSONObject();

        try {
            obj.put("url", "/data/Mattafix1/8c155a57-1ff0-4d1d-a6ae-11a180da1c2c");
            obj.put("data", new JSONObject().put("neco", new JSONObject("KEKET")));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        socket.emit("put", obj, new Ack() {
            @Override
            public void call(Object... args) {
                JSONObject obj = (JSONObject) args[0];
            }
        });*/

        /*
        try {
            socket = IO.socket("http://sandbox.touch4it.com:1341/?__sails_io_sdk_version=0.12.1", opts);
            socket.connect();
        }
        catch (URISyntaxException e) {}


        JSONObject obj = new JSONObject();

        try {
            obj.put("url", "/data/Mattafix1");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        socket.emit("get", obj, new Ack() {
            @Override
            public void call(Object... args) {
                JSONObject obj = (JSONObject) args[0];
            }
        });*/


        //POST
       // SocketHandler socketHandler = new SocketHandler();
      /*  try {
            socketHandler.setSocket(IO.socket("http://sandbox.touch4it.com:1341/?__sails_io_sdk_version=0.12.1", opts));
            socketHandler.getSocket().connect();

        }
        catch (URISyntaxException e) {}*/

/*
        JSONObject obj = new JSONObject();

        try {
            JSONObject jso = new JSONObject();
            JSONArray jarr1=new JSONArray();
            jso.put("Category", "roadBike");
            jso.put("Description", "hejhej");
            jarr1.put(0, jso);



            obj.put("url", "/data/Mattafix");
            obj.put("data", new JSONObject().put("data", jarr1));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        socket.emit("get", obj, new Ack() {
            @Override
            public void call(Object... args) {
                JSONObject obj = (JSONObject) args[0];
                JSONObject body = null;
                JSONArray data = null;

                try {
                    body = obj.getJSONObject("body");
                    data = body.getJSONArray("data");
                    JSONObject dataFromArray;
                    for (int i = 0; i < data.length(); i++){
                        dataFromArray = data.getJSONObject(i);
                        JSONArray datasss = dataFromArray.getJSONArray("data");
                        JSONObject item = datasss.getJSONObject(0);
                        String category = item.getString("Category");
                        Log.i("info", "NECO");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });*/
    }

    /*
    try {
                    JSONObject body = obj.getJSONObject("body");
                    String id = body.getString("id");
                    Log.i("INFO", "NECO");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
     */
    public void onClickEmail (View view){
        EditText email = (EditText) findViewById(R.id.fieldEmail);
        email.setText("");
    }

    public void onClickPass (View view){
        EditText pass = (EditText) findViewById(R.id.fieldPassword);
        pass.setText("");
    }




    public View.OnClickListener createLoginButtonListener()
    {
        return new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                EditText emailField = (EditText) findViewById( R.id.fieldEmail );
                EditText passwordField = (EditText) findViewById( R.id.fieldPassword );

                CharSequence email = emailField.getText();
                CharSequence password = passwordField.getText();



                if( isLoginValuesValid( email, password ) )
                {
                    progress = ProgressDialog.show(MainActivity.this, "Login", "Logging in", true);

                    mAuthTask = new UserLoginTask(emailField.getText().toString(), passwordField.getText().toString());
                    mAuthTask.execute((Void) null);

                }
            }
        };
    }

    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mPassword;

        UserLoginTask(String email, String password) {
            mEmail = email;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {


            try {
                // Simulate network access.
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                return false;
            }

            boolean found = false;

            for (String credential : DUMMY_CREDENTIALS) {
                String[] pieces = credential.split(":");
                if (pieces[0].equals(mEmail) && pieces[1].equals(mPassword)) {
                    // Account exists, return true if the password matches.
                    found = true;
                }
            }
            if (found == true){
                found = false;
                return true;
            }
            else {
                found = false;
                return false;
            }
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            progress.dismiss();

            if (success) {
                received[0] = false;
                setContentView(R.layout.activity_main);
                Intent i = new Intent(getApplicationContext(),CategoryActivity.class);
                startActivity(i);
                finish();
            } else {
                Toast.makeText(getApplicationContext(), getApplicationContext().getString( R.string.warning_incorrect_login_credentials ), Toast.LENGTH_LONG ).show();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            progress.dismiss();
        }
    }


    public boolean isLoginValuesValid( CharSequence email, CharSequence password )
    {
        return Validate.isEmailValid(this, email) && Validate.isPasswordValid( this, password );
    }

    boolean doubleBackToExitPressedOnce = false;
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            moveTaskToBack(true);
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(1);
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
}
