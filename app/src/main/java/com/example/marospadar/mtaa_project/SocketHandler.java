package com.example.marospadar.mtaa_project;


import io.socket.client.Socket;

/**
 * Created by marospadar on 07.05.16.
 */

//trieda pre uchovaanie socket io pripojenia
public class SocketHandler {
    private static Socket socket;

    public static synchronized Socket getSocket(){
        return socket;
    }

    public static synchronized void setSocket(Socket socket){
        SocketHandler.socket = socket;
    }
}