package com.example.marospadar.mtaa_project;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import com.example.marospadar.mtaa_project.backendless.BackendlessRequest;

import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Socket;


public class CategoryActivity extends AppCompatActivity {

    RequestQueue requestQueue;
    ProgressDialog progress;

    LoadingData loadingData;
    GetImages getImages;
    MainActivity mainActivity;
    final boolean[] received = {false};
    final boolean[] serverError = {false};

    JSONArray submissions = new JSONArray();
    ArrayList<String> IDs = new ArrayList<String>();
    JSONArray posts = new JSONArray();

    SQLiteDatabase myDatabase;

    int imagesCount = 0;
    int savedImagesCount = 0;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.category);

        View scooterImage = (View) findViewById( R.id.imageButton );
        View roadBikeImage = (View) findViewById( R.id.imageButton2);
        View enduroImage = (View) findViewById( R.id.imageButton3 );
        View quadBikeImage = (View) findViewById( R.id.imageButton4 );

        scooterImage.setOnClickListener(scooterImageViewButtonListener());
        enduroImage.setOnClickListener(enduroImageViewButtonListener());
        roadBikeImage.setOnClickListener(roadBikeImageViewButtonListener());
        quadBikeImage.setOnClickListener(quadBikeImageViewButtonListener());



        loadData();

    }

    public void loadData(){

        ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo ni = cm.getActiveNetworkInfo();
        //ak zariadenie nie je pripojene na internet obnovia sa offline data a ulozia sa do premennych typu arraylist
        if (ni == null || (!SocketHandler.getSocket().connected())) {
            //first Time use check
            myDatabase = this.openOrCreateDatabase("Items", MODE_PRIVATE, null);
            try {
                Cursor c = myDatabase.rawQuery("SELECT * FROM items", null);
            }
            catch (Exception e){
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(getApplicationContext().getString((R.string.warning_first_time_run)))
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                loadData();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        }
        //ak zariadenie je online stiahnu sa najnovsie data
        else {
            //ked sa vratime z predch. screeny nech nenacitava data znovu


            final DataSingleton dataSingleton = DataSingleton.getInstance();
            //kontrola ci nie je neco na offline upload
            if(dataSingleton.getDataForPUT() != null || dataSingleton.getDataForDELETE() != null || dataSingleton.getDataForPOST() != null)
            {
                int countDataPUT = 0;
                int countDataPOST = 0;
                int countDataDELETE = 0;

                if (dataSingleton.getDataForPUT() == null){
                    countDataPUT = 0;
                }
                else {
                    countDataPUT = dataSingleton.getDataForPUT().size();
                }

                if (dataSingleton.getDataForPOST() == null){
                    countDataPOST = 0;
                }
                else{
                    countDataPOST = dataSingleton.getDataForPOST().size();
                }

                if (dataSingleton.getDataForDELETE() == null){
                    countDataDELETE = 0;
                }
                else{
                    countDataDELETE = dataSingleton.getDataForDELETE().size();
                }


                final int[] completeCountOfUploads = {countDataPUT + countDataPOST + countDataDELETE};
                final int[] actualUploadCount = {0};

                progress = ProgressDialog.show(CategoryActivity.this, "Uploading", "Uploading edited data", true);

                if (dataSingleton.getDataForPUT() != null) {
                    for (int i = 0; i < dataSingleton.getDataForPUT().size(); i++) {

                        SocketHandler.getSocket().emit("put", dataSingleton.getDataForPUT().get(i), new Ack() {
                            @Override
                            public void call(Object... args) {
                                actualUploadCount[0]++;
                                if (actualUploadCount[0] == completeCountOfUploads[0]) {
                                    //vynulovanie listu pre put
                                    dataSingleton.setDataForPOST(null);
                                    dataSingleton.setDataForDELETE(null);
                                    dataSingleton.setDataForPUT(null);
                                    progress.dismiss();
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            loadData();
                                        }
                                    });
                                }
                            }
                        });
                    }
                }
                if(dataSingleton.getDataForDELETE() != null){
                    for (int i = 0; i < dataSingleton.getDataForDELETE().size(); i++) {

                        SocketHandler.getSocket().emit("delete", dataSingleton.getDataForDELETE().get(i), new Ack() {
                            @Override
                            public void call(Object... args) {
                                actualUploadCount[0]++;
                                if (actualUploadCount[0] == completeCountOfUploads[0]) {
                                    //vynulovanie listu pre put
                                    dataSingleton.setDataForPOST(null);
                                    dataSingleton.setDataForDELETE(null);
                                    dataSingleton.setDataForPUT(null);
                                    progress.dismiss();
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            loadData();
                                        }
                                    });
                                }
                            }
                        });
                    }
                }
                if(dataSingleton.getDataForPOST() != null){
                    for (int i = 0; i < dataSingleton.getDataForPOST().size(); i++) {

                        SocketHandler.getSocket().emit("post", dataSingleton.getDataForPOST().get(i), new Ack() {
                            @Override
                            public void call(Object... args) {
                                actualUploadCount[0]++;
                                if (actualUploadCount[0] == completeCountOfUploads[0]) {
                                    //vynulovanie listu pre put
                                    dataSingleton.setDataForPOST(null);
                                    dataSingleton.setDataForDELETE(null);
                                    dataSingleton.setDataForPUT(null);
                                    progress.dismiss();
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            loadData();
                                        }
                                    });
                                }
                            }
                        });
                    }
                }
                return;
            }

            //nacitaj data zo servera a uloz ich do DB
            progress = ProgressDialog.show(CategoryActivity.this, "Loading", "Loading Data", true);
            loadingData = new LoadingData();
            loadingData.execute((Void) null);
        }
    }


    public View.OnClickListener scooterImageViewButtonListener()
    {
        return new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                Intent i = new Intent(CategoryActivity.this, BikeList.class);
                i.putExtra("Category", "scooter");
                startActivity(i);
            }
        };
    }

    public View.OnClickListener enduroImageViewButtonListener()
    {
        return new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                Intent i = new Intent(CategoryActivity.this, BikeList.class);
                i.putExtra("Category", "enduro");
                startActivity(i);
            }
        };
    }

    public View.OnClickListener roadBikeImageViewButtonListener()
    {
        return new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                Intent i = new Intent(CategoryActivity.this, BikeList.class);
                i.putExtra("Category", "roadBike");
                startActivity(i);
            }
        };
    }

    public View.OnClickListener quadBikeImageViewButtonListener()
    {
        return new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                Intent i = new Intent(CategoryActivity.this, BikeList.class);
                i.putExtra("Category", "quadBike");
                startActivity(i);
            }
        };
    }

    //funkcia na ulozenie offline dat do appky predtym sa vsak stare data zmazu
    public void saveAppData(){

        try {
            myDatabase = this.openOrCreateDatabase("Items", MODE_PRIVATE, null);

            myDatabase.execSQL("DROP TABLE IF EXISTS items");
            myDatabase.execSQL("CREATE TABLE IF NOT EXISTS items (title VARCHAR, price VARCHAR, phoneNumber VARCHAR, photo VARCHAR, name VARCHAR, location VARCHAR, description VARCHAR, creationDate VARCHAR, category VARCHAR, objectId VARCHAR, path VARCHAR)");

            JSONObject obj = null;

            for (int i = 0; i < submissions.length() ; i++){

                JSONObject submission = null;
                submission = submissions.getJSONObject(i);

                String title = submission.getString("Title");
                String price = submission.getString("Price");
                String phoneNumber = submission.getString("PhoneNumber");
                String photo = submission.getString("Photo");
                String name = submission.getString("Name");
                String location = submission.getString("Location");
                String description = submission.getString("Description");
               // String creationDate = submission.getString("CreationDate");
                String creationDate = submission.getString("created");
                String category = submission.getString("Category");
                //String objectID = submission.getString("objectId");
                String objectID = IDs.get(i);

                obj = new JSONObject();
                obj.put("Photo", photo);
                obj.put("ObjectId", objectID);

                posts.put(obj);

                myDatabase.execSQL("INSERT INTO items (title, price, phoneNumber, photo, name, location, description, creationdate, category, objectId) VALUES "
                        + "("+"'"+title+"'"+","+"'"+price+"'"+","+"'"+phoneNumber+"'"+","+"'"+photo+"'"+","+"'"+name+"'"+","+"'"+location+"'"+","+"'"+description+"'"+","+"'"+creationDate+"'"+","+"'"+category+"'"+ ","+"'"+objectID+"'" + ")");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }



    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_category, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings_logout) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Are you sure you want to exit application?")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent i = new Intent(getApplicationContext(), MainActivity.class);
                            SocketHandler.getSocket().disconnect();
                            startActivity(i);
                        }
                    })
                    .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
        if (id == R.id.action_settings_refresh) {
            loadData();
        }

        return super.onOptionsItemSelected(item);
    }

    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            moveTaskToBack(true);
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(1);
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    public void loadDataFromServer2(){

        //SocketHandler socketHandler = new SocketHandler();

        JSONObject obj = new JSONObject();

        try {
            obj.put("url", "/data/Mattafix1");

        } catch (JSONException e) {
            e.printStackTrace();
        }


            SocketHandler.getSocket().emit("get", obj, new Ack() {
                @Override
                public void call(Object... args) {
                    JSONObject obj = (JSONObject) args[0];
                    JSONObject body = null;
                    JSONArray data = null;


                    try {
                        body = obj.getJSONObject("body");
                        data = body.getJSONArray("data");
                        JSONObject dataFromArray;
                        for (int i = 0; i < data.length(); i++) {
                            dataFromArray = data.getJSONObject(i);
                            JSONArray datasss = dataFromArray.getJSONArray("data");
                            IDs.add(i, dataFromArray.getString("id"));

                            JSONObject item = datasss.getJSONObject(0);
                            submissions.put(i, item);
                            String category = item.getString("Category");
                            if (category.contains("enduro")) {
                                Data.sharedInstance().enduroResult.add(item);
                            }
                            if (category.contains("scooter")) {
                                Data.sharedInstance().scooterResult.add(item);
                            }
                            if (category.contains("quadBike")) {
                                Data.sharedInstance().quadBikeResult.add(item);
                            }
                            if (category.contains("roadBike")) {
                                Data.sharedInstance().roadBikeResult.add(item);
                            }
                            Log.i("info", "NECO");
                        }

                        saveAppData();

                        received[0] = true;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }}
            });

    }


        public class LoadingData extends AsyncTask<Void, Void, Boolean> {

        LoadingData() {}

        @Override
        protected Boolean doInBackground(Void... params) {

            if (received[0] == true){

            }
            else{
                loadDataFromServer2();
            }
            //tento while je preto, aby appka cakala pokym nepridu data zo servera
            while (received[0] != true){

            }
            if (serverError[0] == true){
                return false;
            }
            else{
                return true;
            }

        }

        @Override
        protected void onPostExecute(final Boolean success) {
            if (success){
                loadingData = null;
                JSONObject submission;

                received[0] = false;
                serverError[0] = false;

                imagesCount = posts.length();

                File dir = new File(Environment.getExternalStorageDirectory()+"/Photos");
                if (dir.isDirectory())
                {
                    String[] children = dir.list();
                    for (int i = 0; i < children.length; i++)
                    {
                        new File(dir, children[i]).delete();
                    }
                }

                for (int i = 0; i < posts.length(); i++) {
                    try {
                        submission = posts.getJSONObject(i);

                        String url = submission.getString("Photo");
                        String objectId = submission.getString("ObjectId");

                        getImages = new GetImages(url, objectId);
                        getImages.execute((Void) null);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                progress.dismiss();
                progress = ProgressDialog.show(CategoryActivity.this, "Loading", "Loading Pictures", true);
            }
            else{
                progress.dismiss();
                AlertDialog alertDialog = new AlertDialog.Builder(CategoryActivity.this).create();
                alertDialog.setTitle("Error While Loading server data");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                serverError[0] = true;
                            }
                        });
                alertDialog.show();
            }
        }

        @Override
        protected void onCancelled() {
            loadingData = null;
            progress.dismiss();
        }
    }

    private class GetImages extends AsyncTask<Object, Object, Object> {
        private String requestUrl, imagename_;
        private ImageView view;
        private Bitmap bitmap ;
        private FileOutputStream fos;

        private GetImages(String requestUrl, String _imagename_) {
            this.requestUrl = requestUrl;
            this.imagename_ = _imagename_ ;
        }

        @Override
        protected Object doInBackground(Object... objects) {
            try {
                URL url = new URL(requestUrl);
                URLConnection conn = url.openConnection();
                bitmap = BitmapFactory.decodeStream(conn.getInputStream());

            } catch (Exception ex) {
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
                if (!ImageStorage.checkifImageExists(imagename_)) {
                    //view.setImageBitmap(bitmap);
                    ImageStorage.saveToSdCard(bitmap, imagename_, myDatabase);
                    savedImagesCount++;
                    if (imagesCount == savedImagesCount) {
                        progress.dismiss();
                        imagesCount = 0;
                        savedImagesCount = 0;
                    }
                } else {
                    savedImagesCount++;
                    if (imagesCount == savedImagesCount) {
                        progress.dismiss();
                        imagesCount = 0;
                        savedImagesCount = 0;
                    }
                }
        }
    }
}
