package com.example.marospadar.mtaa_project;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.marospadar.mtaa_project.backendless.BackendlessRequest;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import io.socket.client.Ack;

public class EditItem extends AppCompatActivity implements ObservableScrollViewCallbacks {


    RequestQueue requestQueue;
    ProgressDialog progress;

    GetImages getImages;

    LoadingData loadingData;

    final boolean[] received = {false};
    final boolean[] serverError = {false};

    JSONArray submissions;
    JSONArray posts = new JSONArray();

    SQLiteDatabase myDatabase;

    int imagesCount = 0;
    int savedImagesCount = 0;

    String objectId;
    String categoryIntent;

    EditText price;
    EditText title;
    EditText description;
    TextView category;
    EditText person;
    EditText location;
    EditText phone;

    ImageView picture;



    public String photoURL = "https://api.backendless.com/574DDB0E-100F-5C2B-FF55-3052A0F85C00/v1/files/Photos/";
    public String photoURL_control = "https://api.backendless.com/574DDB0E-100F-5C2B-FF55-3052A0F85C00/v1/files/Photos/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_item);

        ObservableScrollView scrollView = (ObservableScrollView) findViewById(R.id.scrollView);
        scrollView.setScrollViewCallbacks(this);


        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Button addButton = (Button) findViewById(R.id.EditButton);
        addButton.setOnClickListener(addButtonListener());

        Button addPhotoButton = (Button) findViewById(R.id.editPhotoButton);
        addPhotoButton.setOnClickListener(addPhotoButtonListener());

        //Bundle extras = getIntent().getExtras();
        //categoryIntent =  extras.getString("Category");
        Intent i = getIntent();

        // Selected image id
        objectId = i.getExtras().getString("objectID");
        categoryIntent = i.getExtras().getString("category");



        price = (EditText) findViewById(R.id.itemPriceEdit);
        title = (EditText) findViewById(R.id.itemTitleEdit);
        description = (EditText) findViewById(R.id.descTextViewEdit);
        category = (TextView) findViewById(R.id.itemCategoryEdit);
        person = (EditText) findViewById(R.id.personTextViewEdit);
        location = (EditText) findViewById(R.id.locationTextViewEdit);
        phone = (EditText) findViewById(R.id.phoneTextViewEdit);
        picture = (ImageView) findViewById(R.id.itemPicture);

        loadData();
    }

    public View.OnClickListener addButtonListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if(isValuesValid(price.getText().toString(),
                            title.getText().toString(),description.getText().toString(),person.getText().toString(),location.getText().toString(),
                            phone.getText())) {
                        ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

                        NetworkInfo ni = cm.getActiveNetworkInfo();
                        //ak zariadenie nie je pripojene na internet obnovia sa offline data a ulozia sa do premennych typu arraylist
                        if (ni == null || (!SocketHandler.getSocket().connected())){
                            // tu sa len prida requestque do listu v Datasingleton...
                            progress = ProgressDialog.show(EditItem.this, "Updating", "Updating Data", true);
                            updateOffline();
                        }
                        else{
                            progress = ProgressDialog.show(EditItem.this, "Updating", "Updating Data", true);
                            updateItem();
                        }
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    public View.OnClickListener addPhotoButtonListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // get prompts.xml view
                LayoutInflater li = LayoutInflater.from(EditItem.this);
                View promptsView = li.inflate(R.layout.prompts, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        EditItem.this);

                // set prompts.xml to alertdialog builder
                alertDialogBuilder.setView(promptsView);

                final EditText userInput = (EditText) promptsView
                        .findViewById(R.id.editTextDialogUserInput);

                // set dialog message
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        // get user input and set it to photoURL
                                        // edit text
                                        photoURL = photoURL.concat(userInput.getText().toString());

                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();

            }
        };
    }

    protected void loadData() {

        ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo ni = cm.getActiveNetworkInfo();
        //ak zariadenie nie je pripojene na internet obnovia sa offline data a ulozia sa do premennych typu arraylist
        if (ni == null || (!SocketHandler.getSocket().connected())) {
            myDatabase = this.openOrCreateDatabase("Items", MODE_PRIVATE, null);

            Cursor c = myDatabase.rawQuery("SELECT objectId,category,price,title,phoneNumber,name,location,description,creationDate, path  FROM items WHERE objectId=" + "'" + objectId + "'", null);

            int category_db = c.getColumnIndex("category");
            int price_db = c.getColumnIndex("price");
            int title_db = c.getColumnIndex("title");
            int phoneNumber_db = c.getColumnIndex("phoneNumber");
            int name_db = c.getColumnIndex("name");
            int location_db = c.getColumnIndex("location");
            int description_db = c.getColumnIndex("description");
            int creationDate_db = c.getColumnIndex("creationDate");
            int path_db = c.getColumnIndex("path");

            String path;

            if (c != null && c.moveToFirst()) {
                do {
                    price.setText(c.getString(price_db));
                    description.setText(c.getString(description_db));
                    title.setText(c.getString(title_db));
                    category.setText(c.getString(category_db));
                    person.setText(c.getString(name_db));
                    location.setText(c.getString(location_db));
                    phone.setText(c.getString(phoneNumber_db));

                    path = c.getString(path_db);
                    Bitmap bmp = BitmapFactory.decodeFile(path);
                    picture.setImageBitmap(bmp);


                } while (c.moveToNext());
            }
        }
        //ak zariadenie je online stiahnu sa najnovsie data
        else {
            //ked sa vratime z predch. screeny nech nenacitava data znovu
            requestQueue = VolleySingleton.getInstance(this).getRequestQueue();
            //nacitaj data zo servera a uloz ich do DB
            progress = ProgressDialog.show(EditItem.this, "Loading", "Loading Data", true);
            loadingData = new LoadingData();
            loadingData.execute((Void) null);
        }
    }

    public void updateItem() throws ParseException, JSONException {

        JSONObject obj = new JSONObject();

        try {
            JSONObject jso = new JSONObject();
            JSONArray jarr1=new JSONArray();
            jso.put("Category", categoryIntent);
            jso.put("Description", description.getText().toString());
            jso.put("Title", title.getText().toString());
            jso.put("created", getCurrentDateTime().toString());
            jso.put("Photo", photoURL);
            jso.put("Name", person.getText().toString());
            jso.put("Price", price.getText().toString());
            jso.put("PhoneNumber", phone.getText().toString());
            jso.put("Location", location.getText().toString());

            jarr1.put(0, jso);

            obj.put("url", "/data/Mattafix1/" + objectId);
            obj.put("data", new JSONObject().put("data", jarr1));


        } catch (JSONException e) {
            e.printStackTrace();
        }

        SocketHandler.getSocket().emit("put", obj, new Ack() {
            @Override
            public void call(Object... args) {
                try {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(EditItem.this, "Item: " + title.getText().toString() + " was edited!", Toast.LENGTH_LONG).show();
                            received[0] = true;
                            Intent i = new Intent(getApplicationContext(), CategoryActivity.class);
                            startActivity(i);
                            finish();
                        }
                    });
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void updateOffline()
    {
        final DataSingleton dataSingleton = DataSingleton.getInstance();
        myDatabase.execSQL("UPDATE items SET  title="+"'"+title.getText().toString()+"'"+","+"price="+"'"+price.getText().toString()+"'"+","+"phoneNumber="+"'"+phone.getText().toString()+"'"+","+
                "location="+"'"+location.getText().toString()+"'"+","+"photo="+"'"+photoURL+"'"+","+"description="+"'"+description.getText().toString()+"'"+","+"name="+"'"+person.getText().toString()+"'" + "WHERE objectId = " + "'"+ objectId+"'");

        JSONObject obj = new JSONObject();

        try {
            JSONObject jso = new JSONObject();
            JSONArray jarr1=new JSONArray();
            jso.put("Category", categoryIntent);
            jso.put("Description", description.getText().toString());
            jso.put("Title", title.getText().toString());
            jso.put("created", getCurrentDateTime().toString());
            jso.put("Photo", photoURL);
            jso.put("Name", person.getText().toString());
            jso.put("Price", price.getText().toString());
            jso.put("PhoneNumber", phone.getText().toString());
            jso.put("Location", location.getText().toString());

            jarr1.put(0, jso);

            obj.put("url", "/data/Mattafix1/" + objectId);
            obj.put("data", new JSONObject().put("data", jarr1));


        } catch (JSONException e) {
            e.printStackTrace();
        }

        dataSingleton.setDataForPUT(dataSingleton.addtolistPUT(obj));

        Toast.makeText(EditItem.this, "Item: " + title.getText().toString() + " was edited!", Toast.LENGTH_LONG).show();
        received[0] = true;
        Intent i = new Intent(getApplicationContext(), CategoryActivity.class);
        startActivity(i);

    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll,
                                boolean dragging) {
    }

    @Override
    public void onDownMotionEvent() {
    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
        ActionBar ab = getSupportActionBar();
        if (scrollState == ScrollState.UP) {
            if (ab.isShowing()) {
                ab.hide();
            }
        } else if (scrollState == ScrollState.DOWN) {
            if (!ab.isShowing()) {
                ab.show();
            }
        }
    }

    public class LoadingData extends AsyncTask<Void, Void, Boolean> {

        LoadingData() {
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            if (received[0] == true) {

            } else {
                loadDataFromServer();
            }
            //tento while je preto, aby appka cakala pokym nepridu data zo servera
            while (received[0] != true) {

            }
            if (serverError[0] == true) {
                return false;
            } else {
                return true;
            }

        }

        @Override
        protected void onPostExecute(final Boolean success) {
            if (success) {
                loadingData = null;
                progress.dismiss();

            } else {
                progress.dismiss();
                AlertDialog alertDialog = new AlertDialog.Builder(EditItem.this).create();
                alertDialog.setTitle("Error While Loading server data");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                serverError[0] = true;
                            }
                        });
                alertDialog.show();
            }
        }
    }

    public void loadDataFromServer() {


        JSONObject obj = new JSONObject();

        try {
            obj.put("url", "/data/Mattafix1/"+objectId);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        SocketHandler.getSocket().emit("get", obj, new Ack() {
            @Override
            public void call(Object... args) {
                JSONObject obj = (JSONObject) args[0];
                JSONObject body = null;
                JSONArray data = null;


                try {
                    body = obj.getJSONObject("body");
                    data = body.getJSONArray("data");
                    final JSONObject dataFromArray;
                    dataFromArray = data.getJSONObject(0);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                price.setText(dataFromArray.getString("Price"));
                                description.setText(dataFromArray.getString("Description"));
                                title.setText(dataFromArray.getString("Title"));
                                category.setText(dataFromArray.getString("Category"));
                                person.setText(dataFromArray.getString("Name"));
                                location.setText(dataFromArray.getString("Location"));
                                phone.setText(dataFromArray.getString("PhoneNumber"));
                                String url = dataFromArray.getString("Photo");
                                ifFileExist(url);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });


                    received[0] = true;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void ifFileExist(String url) {

        File extStore = Environment.getExternalStorageDirectory();
        File myFile = new File(extStore.getAbsolutePath() + "/Photos/" + objectId + ".jpg");

        if (myFile.exists()) {
            String path = myFile.getAbsolutePath();
            Bitmap bmp = BitmapFactory.decodeFile(path);
            picture.setImageBitmap(bmp);
        } else {
            getImages = new GetImages(url);
            getImages.execute((Void) null);
        }
    }

    private class GetImages extends AsyncTask<Object, Object, Object> {
        private String requestUrl, imagename_;
        private ImageView view;
        private Bitmap bitmap;
        private FileOutputStream fos;

        private GetImages(String requestUrl) {
            this.requestUrl = requestUrl;
        }

        @Override
        protected Bitmap doInBackground(Object... objects) {
            try {
                URL url = new URL(requestUrl);
                URLConnection conn = url.openConnection();
                bitmap = BitmapFactory.decodeStream(conn.getInputStream());
            } catch (Exception ex) {
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Object o) {
            if (o != null) {
                picture.setImageBitmap(bitmap);
            } else {
                //error while get picture
            }
        }
    }

    public String getCurrentDateTime() {

        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        //get current date time with Date()
        Date date = new Date();
        String dateString = dateFormat.format(date).toString();

        return dateString;
    }

    //MENU
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    //PRAVE HORNE MENU CLICK
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.action_settings_logout:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Do you want to leave editing Item without saving changes ?")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                SocketHandler.getSocket().disconnect();
                                startActivity(i);
                                finish();
                            }
                        })
                        .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();

        }
        return super.onOptionsItemSelected(item);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    protected void exitByBackKey() {

        AlertDialog alertbox = new AlertDialog.Builder(this)
                .setMessage("Do you want to leave editing Item without saving changes ?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                })
                .show();

    }

    public boolean isValuesValid(String price, String title, String description, String person, String location, CharSequence phone) {
        return Validate.isNumber(this, price) && Validate.isEmptyString(this, title)
                && Validate.isEmptyString(this, description) && Validate.isEmptyString(this, person)
                && Validate.isEmptyString(this, location) && Validate.isValidPhoneNumber(this, phone);
    }
}