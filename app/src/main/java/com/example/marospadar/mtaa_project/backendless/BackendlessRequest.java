package com.example.marospadar.mtaa_project.backendless;

import com.android.volley.Response;

/**
 * Created by Martin on 24/03/16.
 */

public class BackendlessRequest {

    public BackendlessCoreRequest getRequest() {
        return request;
    }

    public BackendlessCoreRequest request;

    public BackendlessRequest(int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        this.request = new BackendlessCoreRequest(method, url, listener, errorListener);

    }



}
