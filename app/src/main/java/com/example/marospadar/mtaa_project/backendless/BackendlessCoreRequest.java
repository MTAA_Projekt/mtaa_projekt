package com.example.marospadar.mtaa_project.backendless;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Martin on 24/03/16.
 */
public class BackendlessCoreRequest extends StringRequest {

    /**
     * Holds the last part of url
     * eg. "/revizor"
     */
    private String entity;
    private Map<String, String> params;
    private byte [] body;

    private String url = "https://api.backendless.com/v1/data";
    private String appId = "574DDB0E-100F-5C2B-FF55-3052A0F85C00";
    private String secret = "064A3F2E-D3BB-2F6C-FFCE-1A7619CF6900";



    public BackendlessCoreRequest(int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(method, "", listener, errorListener);
        this.entity = url;

    }

    @Override
    public String getUrl() {
        return this.url + this.entity;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    public void setBody(String body) {
        this.body = body.getBytes();
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String>  params = new HashMap<String, String>();
        params.put("application-id", this.appId);
        params.put("secret-key", this.secret);
        return params;
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return super.getParams();
    }
}
