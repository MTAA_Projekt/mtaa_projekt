package com.example.marospadar.mtaa_project;

import android.content.Context;
import android.text.TextUtils;
import android.util.Patterns;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Martin on 16/03/16.
 */
public class Validate {

    /**
     * Validates user's name: checks whether it is not empty and whether the first letter is in uppercase.
     * Shows Toast with a warning if validation fails.
     *
     * @param currentContext context, in which validation occurs
     * @param name           user's name to be validated
     * @return true if name is valid, false if validation failed
     */
    public static boolean isNameValid( Context currentContext, CharSequence name )
    {
        if( name.toString().isEmpty() )
        {
            Toast.makeText(currentContext, currentContext.getString(R.string.warning_name_empty), Toast.LENGTH_LONG).show();
            return false;
        }

        if( !Character.isUpperCase(name.charAt(0)) )
        {
            Toast.makeText( currentContext, currentContext.getString( R.string.warning_name_lowercase ), Toast.LENGTH_LONG ).show();
            return false;
        }

        return true;
    }

    /**
     * Validates email: checks whether it is not empty and whether it matches Patterns.EMAIL_ADDRESS regex.
     * Shows Toast with a warning if validation fails.
     *
     * @param currentContext context, in which validation occurs
     * @param email          email to be validated
     * @return true if email is valid, false if validation failed
     */
    public static boolean isEmailValid( Context currentContext, CharSequence email )
    {
        if( email.toString().isEmpty() )
        {
            Toast.makeText( currentContext, currentContext.getString( R.string.warning_email_empty ), Toast.LENGTH_LONG ).show();
            return false;
        }

        if( !Patterns.EMAIL_ADDRESS.matcher( email ).matches() )
        {
            Toast.makeText( currentContext, currentContext.getString( R.string.warning_email_invalid ), Toast.LENGTH_LONG ).show();
            return false;
        }

        return true;
    }

    /**
     * Validates password: checks whether it is not empty.
     * Shows Toast with a warning if validation fails.
     *
     * @param currentContext context, in which validation occurs
     * @param password       password to be validated
     * @return true if password is valid, false if validation failed
     */
    public static boolean isPasswordValid( Context currentContext, CharSequence password )
    {
        if( password.toString().isEmpty() )
        {
            Toast.makeText( currentContext, currentContext.getString( R.string.warning_password_empty ), Toast.LENGTH_LONG ).show();
            return false;
        }

        return true;
    }

    public static boolean isEmptyString(Context currentContext, String text) {
        if (text == null || text.trim().equals("null") || text.trim()
                .length() <= 0){
            Toast.makeText( currentContext,  currentContext.getString( R.string.warning_empty_value ), Toast.LENGTH_LONG ).show();
            return false;
        }
        return true;
    }

    public static boolean isValidPhoneNumber(Context currentContext, CharSequence phoneNumber) {
        if ((!TextUtils.isEmpty(phoneNumber)) && Patterns.PHONE.matcher(phoneNumber).matches()) {
            return true;
        }
        else{
            Toast.makeText( currentContext,  currentContext.getString( R.string.warning_invalid_phone_number ), Toast.LENGTH_LONG ).show();
            return false;
        }
    }

    public static boolean isNumber(Context currentContext, String string) {
        if(!(string.matches("^\\d+$"))) {
            Toast.makeText(currentContext, currentContext.getString(R.string.warning_no_number), Toast.LENGTH_LONG).show();
            return false;
        }
        return true;

    }






}
