package com.example.marospadar.mtaa_project;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import android.graphics.Matrix;
import android.graphics.PointF;
import android.util.FloatMath;
import android.view.MotionEvent;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.marospadar.mtaa_project.backendless.BackendlessRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.net.URLConnection;

import io.socket.client.Ack;

public class DetailImageView extends AppCompatActivity implements View.OnTouchListener{


    RequestQueue requestQueue;
    ProgressDialog progress;

    GetImages getImages;

    LoadingData loadingData;

    final boolean[] received = {false};
    final boolean[] serverError = {false};

    JSONArray submissions;
    JSONArray posts = new JSONArray();

    SQLiteDatabase myDatabase;

    int imagesCount = 0;
    int savedImagesCount = 0;

    String objectId;

    ImageView picture;

    //pinch to zoom try
    private static final String TAG = "Touch";

    // These matrices will be used to move and zoom image
    Matrix matrix = new Matrix();
    Matrix savedMatrix = new Matrix();

    // We can be in one of these 3 states
    static final int NONE = 0;
    static final int DRAG = 1;
    static final int ZOOM = 2;
    int mode = NONE;

    // Remember some things for zooming
    PointF start = new PointF();
    PointF mid = new PointF();
    float oldDist = 1f;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_image_view);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent i = getIntent();

        requestQueue = VolleySingleton.getInstance(this).getRequestQueue();

        // Selected image id
        objectId = i.getExtras().getString("ObjectId");

        picture = (ImageView) findViewById(R.id.detailImageView);
        picture.setScaleType(ImageView.ScaleType.FIT_CENTER); // make the image fit to the center.
        picture.setOnTouchListener(this);
        loadData();


    }

    protected void loadData() {

        ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo ni = cm.getActiveNetworkInfo();
        //ak zariadenie nie je pripojene na internet obnovia sa offline data a ulozia sa do premennych typu arraylist
        if (ni == null || (!SocketHandler.getSocket().connected())) {
            myDatabase = this.openOrCreateDatabase("Items", MODE_PRIVATE, null);

            Cursor c = myDatabase.rawQuery("SELECT path FROM items WHERE objectId=" + "'" + objectId + "'", null);

            int path_db = c.getColumnIndex("path");

            String path;

            if (c != null && c.moveToFirst()) {
                do {

                    path = c.getString(path_db);
                    Bitmap bmp = BitmapFactory.decodeFile(path);
                    picture.setImageBitmap(bmp);


                } while (c.moveToNext());
            }
        }
        //ak zariadenie je online stiahnu sa najnovsie data
        else {
            progress = ProgressDialog.show(DetailImageView.this, "Loading", "Loading Data", true);
            loadingData = new LoadingData();
            loadingData.execute((Void) null);
        }
    }

    public void ifFileExist(String url){

        File extStore = Environment.getExternalStorageDirectory();
        File myFile = new File(extStore.getAbsolutePath() + "/Photos/"+objectId+".jpg");

        if(myFile.exists()){
            String path = myFile.getAbsolutePath();
            Bitmap bmp = BitmapFactory.decodeFile(path);
            picture.setImageBitmap(bmp);
        }
        else{
            getImages = new GetImages(url);
            getImages.execute((Void) null);
        }
    }

    private class GetImages extends AsyncTask<Object, Object, Object> {
        private String requestUrl, imagename_;
        private ImageView view;
        private Bitmap bitmap ;
        private FileOutputStream fos;

        private GetImages(String requestUrl) {
            this.requestUrl = requestUrl;
        }

        @Override
        protected Bitmap doInBackground(Object... objects) {
            try {
                URL url = new URL(requestUrl);
                URLConnection conn = url.openConnection();
                bitmap = BitmapFactory.decodeStream(conn.getInputStream());
            } catch (Exception ex) {
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Object o) {
            if(o != null)
            {
                picture.setImageBitmap(bitmap);
            }
            else {
                //error while get picture
            }
        }
    }

    //PRAVE HORNE MENU CLICK
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void loadDataFromServer(){

        JSONObject obj = new JSONObject();

        try {
            obj.put("url", "/data/Mattafix1/"+objectId);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        SocketHandler.getSocket().emit("get", obj, new Ack() {
            @Override
            public void call(Object... args) {
                JSONObject obj = (JSONObject) args[0];
                JSONObject body = null;
                JSONArray data = null;


                try {
                    body = obj.getJSONObject("body");
                    data = body.getJSONArray("data");
                    final JSONObject dataFromArray;
                    dataFromArray = data.getJSONObject(0);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String url = dataFromArray.getString("Photo");
                                ifFileExist(url);
                                received[0] = true;
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });


                    received[0] = true;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public class LoadingData extends AsyncTask<Void, Void, Boolean> {

        LoadingData() {
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            if (received[0] == true) {

            } else {
                loadDataFromServer();
            }
            //tento while je preto, aby appka cakala pokym nepridu data zo servera
            while (received[0] != true) {

            }
            if (serverError[0] == true) {
                return false;
            } else {
                return true;
            }

        }

        @Override
        protected void onPostExecute(final Boolean success) {
            if (success) {
                loadingData = null;
                progress.dismiss();

            } else {
                progress.dismiss();
                AlertDialog alertDialog = new AlertDialog.Builder(DetailImageView.this).create();
                alertDialog.setTitle("Error While Loading server data");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                serverError[0] = true;
                            }
                        });
                alertDialog.show();
            }
        }
    }

    public boolean onTouch(View v, MotionEvent event) {
        ImageView view = (ImageView) v;
        // make the image scalable as a matrix
        view.setScaleType(ImageView.ScaleType.MATRIX);
        float scale;

        // Handle touch events here...
        switch (event.getAction() & MotionEvent.ACTION_MASK) {

            case MotionEvent.ACTION_DOWN: //first finger down only
                savedMatrix.set(matrix);
                start.set(event.getX(), event.getY());
                Log.d(TAG, "mode=DRAG" );
                mode = DRAG;
                break;
            case MotionEvent.ACTION_UP: //first finger lifted
            case MotionEvent.ACTION_POINTER_UP: //second finger lifted
                mode = NONE;
                Log.d(TAG, "mode=NONE" );
                break;
            case MotionEvent.ACTION_POINTER_DOWN: //second finger down
                oldDist = spacing(event); // calculates the distance between two points where user touched.
                Log.d(TAG, "oldDist=" + oldDist);
                // minimal distance between both the fingers
                if (oldDist > 5f) {
                    savedMatrix.set(matrix);
                    midPoint(mid, event); // sets the mid-point of the straight line between two points where user touched.
                    mode = ZOOM;
                    Log.d(TAG, "mode=ZOOM" );
                }
                break;

            case MotionEvent.ACTION_MOVE:
                if (mode == DRAG)
                { //movement of first finger
                    matrix.set(savedMatrix);
                    if (view.getLeft() >= -392)
                    {
                        matrix.postTranslate(event.getX() - start.x, event.getY() - start.y);
                    }
                }
                else if (mode == ZOOM) { //pinch zooming
                    float newDist = spacing(event);
                    Log.d(TAG, "newDist=" + newDist);
                    if (newDist > 5f) {
                        matrix.set(savedMatrix);
                        scale = newDist/oldDist; //thinking I need to play around with this value to limit it**
                        matrix.postScale(scale, scale, mid.x, mid.y);
                    }
                }
                break;
        }

        // Perform the transformation
        view.setImageMatrix(matrix);

        return true; // indicate event was handled
    }

    private float spacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return (float)Math.sqrt(x * x + y * y);
    }

    private void midPoint(PointF point, MotionEvent event) {
        float x = event.getX(0) + event.getX(1);
        float y = event.getY(0) + event.getY(1);
        point.set(x / 2, y / 2);
    }

}
